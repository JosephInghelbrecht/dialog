<?php

namespace ModernWays\Dialog\Model;

/**
 * Notice class
 * @lastmodified 13/12/2017
 * @since 13/12/2017
 * @author Entreprise de Modes et de Manieres Modernes - e3M
 * @version 0.1
 */
class Notice implements INotice
{
    private $start;
    private $end;
    private $code;
    private $subject;
    private $message;
    private $source;

    public function clone()
    {
        $copy = new Notice();
        $copy->setStart($this->start);
        $copy->setEnd($this->end);
        $copy->setCode($this->code);
        $copy->setSubject($this->subject);
        $copy->setMessage($this->message);
        $copy->setSource($this->source);
        return $copy;
    }

    public function setStart($value = null)
    {
        if (is_null($value)) {
            $this->start = date("Y/m/d H:i:s");
        } else {
            $this->start = $value;
        }
    }

    public function setEnd($value = null)
    {
        if (is_null($value)) {
            $this->end = date("Y/m/d H:i:s");
        } else {
            $this->end = $value;
        }
    }

    public function setCode($value)
    {
        $this->code = $value;
    }

    public function setSubject($value)
    {
        $this->subject = $value;
    }

    public function setMessage($value)
    {
        $this->message = $value;
    }

    public function setSource($value)
    {
        $this->source = $value;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getSource()
    {
        return $this->source;
    }

}