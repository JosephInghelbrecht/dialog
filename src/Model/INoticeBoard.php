<?php 
namespace ModernWays\Dialog\Model;

    /** 
    * NoticeBoard Interface 
    * @lastmodified 13/12/2017 
    * @since 13/12/2017             
    * @author Entreprise de Modes et de Manieres Modernes - e3M        
    * @version 0.1 
    */  
    Interface INoticeBoard {
        function setTitle($value);
        function getTitle();
        function __construct($title);

        function pin();
        function unpin();
        function getBoard();
    }
        