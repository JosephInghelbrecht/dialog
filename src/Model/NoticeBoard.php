<?php 
namespace ModernWays\Dialog\Model;

    /** 
    * NoticeBoard class 
    * @lastmodified 13/12/2017 
    * @since 13/12/2017             
    * @author Entreprise de Modes et de Manieres Modernes - e3M        
    * @version 0.1 
    */  
    class NoticeBoard extends Notice implements INoticeBoard {
        private $title;
        private $board = array();

        function __construct($title)
        {
            $this->title = $title;
        }

        public function setTitle($value) {
            $this->title = $value;
        }
        
        public function getTitle() {
            return $this->title;
        }
        
        public function pin() {
            $this->board[] = $this->clone();
        }
        
        public function unpin() {
        }
        
        public function getBoard() {
            return $this->board;
        }
    }
        