<?php
namespace ModernWays\Dialog\Model;
// automatische document generator zoals de drie backslashes in c#
    /** 
    * Notice Interface.
    * @lastmodified 13/12/2017 
    * @since 13/12/2017            
    * @author Entreprise Anne-Marie Misson        
    * @version 0.1 
    */  
    Interface INotice {
        function setStart($value = null);
        function setEnd($value = null);
        function setCode($value);
        function setSubject($value);
        function setMessage($value);
        function setSource($value);

        function getStart();
        function getEnd();
        function getCode();
        function getSubject();
        function getMessage();

        function clone();
    }
?>