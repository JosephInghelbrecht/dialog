<div>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="command-bar">
            <button type="submit" name="uc" value="Notice/Insert">
            Insert in de tabel
        </button>
        </div>

        <div>
            <label for="Subject">Subject</label>
            <input type="text" name="Subject" required/>
        </div>
        <div>
            <label for="Code">Code</label>
            <input type="text" name="Code" />
        </div>

        <div>
            <label for="Message">Message</label>
            <input type="text" name="Message" />
        </div>

        <div>
            <label for="Start">Start</label>
            <input type="date" name="Start" />
        </div>

        <div>
            <label for="End">End</label>
            <input type="date" name="End" />
        </div>

        <div>
            <label for="Source">Source</label>
            <input type="text" name="Source" />
        </div>
    </form>
</div>
